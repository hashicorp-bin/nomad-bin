#!/usr/bin/env node
'use strict';

const { spawn } = require('child_process');
const { resolve } = require('path');

//const execName = 
console.log(process.cwd())
const command = '/usr/bin/java -jar /Users/ethanbrooks/Documents/tabula-bin/tools/tabula.jar --area "112.85422740524781,6.244897959183674,207.4198250728863,571.8542274052478" ../tests/test.pdf';
const tabula = spawn(command, process.argv.slice(2), { cwd: process.cwd() });



tabula.stdout.pipe(process.stdout);
tabula.stderr.pipe(process.stderr);
tabula.on('error', function(err) {
  console.error(`Received an error while executing the tabula binary: ${err}`);
  process.exit(1);
});
