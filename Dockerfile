FROM node:12-buster-slim
RUN mkdir -p /usr/share/man/man1 && apt-get update && apt-get install -y --no-install-recommends \
    default-jre \
    default-jdk \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-get install -y --no-install-recommends \
    python3 \
    python3-pip \
    build-essential \
    && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*